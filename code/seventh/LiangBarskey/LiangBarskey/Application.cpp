#include <GL/glut.h>
#include <GL/gl.h>
#include <math.h>
#include <stdio.h>

class wcPt2D 
{
private:
	GLfloat x, y;
public:
	void wcPt3D() {
		x = y = 0.0;
	}

	void setCoords (GLfloat xCoord, GLfloat yCoord) {
		x = xCoord;
		y = yCoord;
	}
	
	GLfloat getx() const {
		return x;
	}

	GLfloat gety() const {
		return y;
	}

};

inline GLint round(const GLfloat a) {
	return GLint(a + 0.5);
}

GLint clipTest(GLfloat p, GLfloat q, GLfloat* u1, GLfloat* u2) {
	GLfloat r;
	GLint returnValue = true;

	if (p < 0.0)
	{
		r = q / p;
		if (r>*u2)
		{
			returnValue = false;
		}
		else
		{
			if (r>*u1)
			{
				*u1 = r;
			}
		}
	}
	else {
		if (p > 0.0)
		{
			r = q / p;
			if (r < *u1)
			{
				returnValue = false;
			}
			else if (r < *u2)
			{
				*u2 = r;
			}
		}
		else
		{
			if (q<0.0)
			{
				returnValue = false;
			}
		}
	}
	return (returnValue);
}

// DDA算法
void lineDDA(int x0, int y0, int xend, int yend) {
	int dx = xend - x0, dy = yend - y0; // 两点间的横,纵坐标间隔
	float xIncrement, yIncrement, x = x0, y = y0; // 初始化x,y
	int steps;

	if (fabs(dx) >= dy) { // 当斜率小于等于1时
		steps = fabs(dx);
	}
	else // 当斜率大小大于1时
	{
		steps = fabs(dy);
	}
	// 设置 Δx, Δy 的大小
	xIncrement = float(dx) / float(steps);
	yIncrement = float(dy) / float(steps);

	//开始画点
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);
	glVertex2i(x0, y0);
	for (int i = 0; i < steps; i++)
	{
		x += xIncrement;
		y += yIncrement;
		glVertex2f(x, y);
	}
	glVertex2i(xend, yend);
	glEnd();
	glFlush();
}

// Bresenham 算法
void lineBresenham(int x0, int y0, int xend, int yend) {
	int dx = fabs(xend - x0), dy = fabs(yend - y0);
	int p = 2 * dy - dx;
	int twoDy = 2 * dy, twoDyMinusDx = 2 * (dy - dx);
	int x, y;
	// 确定开始位置
	if (x0 > xend) // x0在右侧
	{
		x = xend;
		y = yend;
		xend = x0;
	}
	else // x0在左侧
	{
		x = x0;
		y = y0;
	}
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);
	glVertex2f(x, y);
	while (x < xend)
	{
		x++;
		if (p < 0)
		{
			p += twoDy;
		}
		else {
			y++;
			p += twoDyMinusDx;
		}
		glVertex2f(x, y);
	}
	glEnd();
	glFlush();
}

void lineClipLIangBarsk(wcPt2D winMin, wcPt2D winMax, wcPt2D p1, wcPt2D p2) {

	GLfloat u1 = 0.0, u2 = 1.0, dx = p2.getx() - p1.getx(), dy;

	if (clipTest(-dx,p1.getx()- winMin.getx(),&u1,&u2)) {
		if (clipTest(dx,winMax.getx()-p1.getx(),&u1,&u2))
		{
			dy = p2.gety() - p1.gety();

			if (clipTest(dy, winMax.gety() - p1.gety(),&u1,&u2))
			{
				if (u2 < 1.0)
				{
					p2.setCoords(p1.getx() + u2 * dx, p1.gety()+u2 * dy);
				}
				if (u1 > 0.0)
				{
					p1.setCoords(p1.getx() + u1 * dx, p1.gety() + u1 * dy);
				}
				lineDDA(p1.getx(), p1.gety(), p2.getx(), p2.gety());
			}
		}
	}
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT);
	glViewport(100, 100, 500, 500);
	wcPt2D winMin;
	winMin.setCoords(100,100); // 左上
	wcPt2D maxWin;
	maxWin.setCoords(300, 200); // 右下

	wcPt2D p1;
	p1.setCoords(120, 300);
	wcPt2D p2;
	p2.setCoords(180, 80);
	
	//lineDDA(p1.getx(), p1.gety(), p2.getx(), p2.gety());
	lineClipLIangBarsk(winMin, maxWin, p1,p2);
}

void Init() {
	glClearColor(1.0, 1.0, 1.0, 0.0); // 设置背景
	glPointSize(5.0); // 点的大小
	glColor3f(0.0, 0.0, 0.0);
	gluOrtho2D(0.0, 600.0, 0.0, 600.0);
	glMatrixMode(GL_MODELVIEW);
}

int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("梁-Barsky裁剪算法");
	glutDisplayFunc(display);
	Init();
	glutMainLoop();
}