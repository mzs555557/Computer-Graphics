#include <windows.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <math.h>
#include <stdlib.h>

// DDA算法
void lineDDA(int x0, int y0, int xend, int yend) {
	int dx = xend - x0, dy = yend - y0; // 两点间的横,纵坐标间隔
	float xIncrement, yIncrement, x = x0, y = y0; // 初始化x,y
	int steps;

	if (fabs(dx) >= dy) { // 当斜率小于等于1时
		steps = fabs(dx);
	}
	else // 当斜率大小大于1时
	{
		steps = fabs(dy);
	}
	// 设置 Δx, Δy 的大小
	xIncrement = float(dx) / float(steps);
	yIncrement = float(dy) / float(steps);
	
	//开始画点
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);
	glVertex2i(x0, y0);
	for (int i = 0; i < steps; i++)
	{
		x += xIncrement;
		y += yIncrement;
		glVertex2f(x, y);
	}
	glVertex2i(xend, yend);
	glEnd();
	glFlush();
}

// Bresenham 算法
void lineBresenham(int x0, int y0, int xend, int yend) {
	int dx = fabs(xend - x0), dy = fabs(yend - y0);
	int p = 2 * dy - dx;
	int twoDy = 2 * dy, twoDyMinusDx = 2 * (dy - dx);
	int x, y;
	// 确定开始位置
	if (x0 > xend) // x0在右侧
	{
		x = xend;
		y = yend;
		xend = x0;
	}
	else // x0在左侧
	{
		x = x0;
		y = y0;
	}
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);
	glVertex2f(x, y);
	while (x<xend)
	{
		x++;
		if (p<0)
		{
			p += twoDy;
		}
		else {
			y++;
			p += twoDyMinusDx;
		}
		glVertex2f(x, y);
	}
	glEnd();
	glFlush();
}

// 主方法
void display() {
	glClear(GL_COLOR_BUFFER_BIT);
	glViewport(100, 100, 500, 500);
	//lineDDA(30, 35, 140, 200);
	lineBresenham(40, 45, 140, 135);
}

// 初始化该方法
void Init() {
	glClearColor(1.0, 1.0, 1.0, 0.0); // 设置背景
	glPointSize(5.0); // 点的大小
	glColor3f(0.0, 0.0, 0.0);
	gluOrtho2D(0.0, 600.0, 0.0, 600.0);
	glMatrixMode(GL_MODELVIEW);
}

int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("lineDDA-Bresenham");
	glutDisplayFunc(display);
	Init();
	glutMainLoop();

	return 0;
}


