#include <windows.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <iostream>
#include <cmath>

using namespace std;

void swapvalue(int* a, int* b);//函数的预定义
int calculateDistance(int x1, int y1, int x2, int y2);

void DrawBresh(int x1, int y1, int x2, int y2) {
	glColor3f(0.0, 0.0, 1.0);
	glPointSize(2.0f);
	int dx = abs(x2 - x1);
	int dy = abs(y2 - y1);

	if (dx == 0 && dy == 0)
	{
		glBegin(GL_POINTS);
		glVertex2f(x1, y1);
		glEnd();
		glFlush();
		return;
	}

	int flag = 0;
	if (dx < dy)
	{
		flag = 1;
		swapvalue(&x1, &y1);
		swapvalue(&x2, &y2);
		swapvalue(&dx, &dy);
	}

	int tx = (x2 - x1) > 0 ? 1 : -1;
	int ty = (y2 - y1) > 0 ? 1 : -1;
	int curx = x1;
	int cury = y1;
	int dS = 2 * dy;
	int dT = 2 * (dy - dx);
	int d = dS - dx;

	while (curx != x2)
	{
		if (d<0)
		{
			d += dS;
		}
		else
		{
			cury += ty;
			d += dT;
		}
		if (flag)
		{
			glBegin(GL_POINTS);
			glVertex2f(curx, cury);
			glEnd();
			glFlush();
		}
		else {
			glBegin(GL_POINTS);
			glVertex2f(curx, cury);
			glEnd();
			glFlush();
		}
		curx += tx;
	}
}

void swapvalue(int* a, int* b) {
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void display(void) {
	glClear(GL_COLOR_BUFFER_BIT);
	int x1 = 0, y1 = 0, x2 = 500, y2 = 500;
	DrawBresh(x1, y1, x2, y2);
	
	int distance = calculateDistance(x1, y1, x2, y2);

	cout << distance << endl;
}

int calculateDistance(int x1, int y1, int x2, int y2) {
	int dx = abs(x1 - x2);
	int dy = abs(y1 - y2);
	int distance = sqrt(dx * dx + dy * dy);
	return distance;
}

int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowPosition(200, 200);
	glutInitWindowSize(600, 600);
	glutCreateWindow("作业");
	glutDisplayFunc(display);
	glClearColor(1.0, 1.0, 1.0, 0.0);  //窗口的背景为白色
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(0.0, 200.0, 0.0, 150.0);
	glutMainLoop();//让 glut 程序进入事件循环


	return 0;
}