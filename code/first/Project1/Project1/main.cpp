

#include <iostream>
#include <math.h>
#include <gl\glut.h>
#include <gl\GL.h>
#include <gl\GLU.h>
using namespace std;
#define pi 3.1415926

void init(void)
{
	glClearColor(1.0, 1.0, 1.0, 0.0);  // Set display-window color to white.
	glMatrixMode(GL_PROJECTION);       // Set projection parameters.
	gluOrtho2D(0.0, 200.0, 0.0, 150.0);
}


//窗口大小改变时调用的登记函数
void ChangeSize(GLsizei w, GLsizei h) {
	if (h == 0)
		h = 1;
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (w <= h)
		glOrtho(0.0f, 250.0f, 0.0f, 250.0f * h / w, 1.0, -1.0);
	else
		glOrtho(0.0f, 250.0f * w / h, 0.0f, 250.0f, 1.0, -1.0);
}

void CirclePoint(int x0, int y0, int x, int y) {
	glColor3f(0.0, 0.0, 1.0);  //设置像素点颜色为蓝色
	//按顺时针画圆
	glVertex2f(x, y); glVertex2f(y - (y0 - x0), x + (y0 - x0));
	glVertex2f(y - (y0 - x0), x0 + y0 - x); glVertex2f(x, 2 * y0 - y);
	glVertex2f(2 * x0 - x, 2 * y0 - y); glVertex2f(x0 + y0 - y, x0 + y0 - x);
	glVertex2f(x0 + y0 - y, x + (y0 - x0)); glVertex2f(2 * x0 - x, y);
}

//中点画圆算法
void MidPointCircle(int x0, int y0, int r)
{
	glColor3f(0.0, 0.0, 1.0);  //设置像素点颜色为蓝色
	glPointSize(2.0f);         //设置像素点大小
	glVertex2f(x0, y0);        //画圆心
	int x, y;
	int d, b;
	x = x0; y = r + y0; d = 5 - 4 * r; b = y0 - x0;
	CirclePoint(x0, y0, x, y);
	while (x + b <= y)
	{
		if (d < 0)
			d += 8 * (x - x0) + 12;
		else {
			d += 8 * x - 8 * y + 8 * b + 16;
			y--;
		}
		x++;
		CirclePoint(x0, y0, x, y);
	}

}

//极坐标画圆算法
void PolarCircle(int x0, int y0, int r)
{
	glColor3f(0.0, 0.0, 1.0);  //设置像素点颜色为蓝色
	glPointSize(2.0f);         //设置像素点大小
	glVertex2f(x0, y0);        //画圆心
	int angle = 90;
	int x, y, change;
	while (angle >= 45)
	{
		//角度转弧度
		change = angle * pi / 180;
		x = int(0.5 + x0 + r * cos(change));
		y = int(0.5 + y0 + r * sin(change));
		CirclePoint(x0, y0, x, y);
		angle--;
	}
}

void display()
{
	int x1_1 = 20, y1_1 = 20, x2_1 = 160, y2_1 = 80;
	int x1_2 = 20, y1_2 = 40, x2_2 = 160, y2_2 = 100;
	int x1_3 = 20, y1_3 = 60, x2_3 = 160, y2_3 = 120;
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);
	MidPointCircle(80, 60, 60);
	glEnd();
	glBegin(GL_POINTS);
	MidPointCircle(80, 80, 60);
	glEnd();
	glFlush();
}


int main(int argc, char** argv)
{
	glutInit(&argc, argv); //GLUT初始化
	glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE); //指出显示窗口使用单个缓存且使用由红绿蓝三种颜色模型
	glutInitWindowPosition(500, 500);            //设置显示窗口位置
	glutInitWindowSize(400, 400);                //设置显示窗口大小
	glutCreateWindow("Line Algorithm");           //设置显示窗口的标题
	glutDisplayFunc(display);                    //将图赋值给显示窗口
	glutReshapeFunc(ChangeSize);
	init();
	glutMainLoop();              //必须是程序的最后一个
	return 0;
}
