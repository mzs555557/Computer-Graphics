#include "DisplayManager.h"

void DisplayManager::createDisplay()
{
	
	mDisplay.setTitle("first window");
	mDisplay.setDisplayMode(DisplayMode(640, 480));
	ContestAttri attr(4, 6);
	
	mDisplay.create(attr);
}

void DisplayManager::updateDisplay()
{
	mDisplay.update();
}

void DisplayManager::closeDisplay()
{
	mDisplay.detroy();
}

bool DisplayManager::isRequestClosed()
{
	return mDisplay.isRequestClosed();
}
