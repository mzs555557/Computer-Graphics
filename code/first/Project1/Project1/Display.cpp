#include "Display.h"
#include "GLFW/glfw3.h"
#include <iostream>

void Display::create(ContestAttri attr)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, attr.major);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, attr.minor);

	if (attr.bProfileCore)
	{
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_API);
	}

	mWindow = glfwCreateWindow(mDisplayMode.width, mDisplayMode.height, title, nullptr, nullptr);
	
	if (!mWindow)
	{	
		std::cout << "create window failed" << std::endl;
		return;
	}

	glfwMakeContextCurrent(mWindow);

	glfwSetFramebufferSizeCallback(mWindow, frameBufferSizeCallback);
}

void Display::update()
{
	processEvent();

	glfwPollEvents();
	glfwSwapBuffers(mWindow);
}

void Display::detroy()
{
	glfwDestroyWindow(mWindow);
	glfwTerminate();
	mWindow = nullptr;
}

bool Display::isRequestClosed()
{
	if (glfwWindowShouldClose(mWindow)) {
		this->detroy();
		return true;
	}
	return false;
}

void Display::setDisplayMode(DisplayMode mode)
{
	mDisplayMode = mode;
}

void Display::setTitle(const char* _title)
{

}

void Display::frameBufferSizeCallback(GLFWwindow* _window, int, int)
{

}

void Display::processEvent()
{
	if (glfwGetKey(mWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(mWindow, true);
	
	}
}
