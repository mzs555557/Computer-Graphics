#include <GL/glut.h>
#include <stdlib.h>
#include <math.h>

GLfloat xwcMin = -250.0, xwcMax = 250.0;
GLfloat ywcMin = -250.0, ywcMax = 250.0;

class WcPt3d {
public:
	GLfloat x, y, z;
};

// 画点
void plotPt(WcPt3d point) {
	glBegin(GL_POINTS);
	glVertex3f(point.x,point.y,point.z);
	glEnd();
}

// 初始化背景色
void init(void) {
	glClearColor(1.0, 1.0, 1.0, 0.0);
}


// 计算二项式的系数 ***
void BinomialCoeffi(GLint *C, GLint ctrlNums) {
	GLint k, j;
	for ( k = 0; k <= ctrlNums; k++)
	{
		C[k] = 1;
		// n!/(k!(n-k)!)
		for ( j = ctrlNums; j >= k+1 ; j--) // n!/k!
		{
			C[k] *= j;
		}

		for (j = ctrlNums - k; j >= 2; j--) // (n-k)!
		{
			C[k] /= j;
		}
	}
}

void computeBezierPts(GLfloat t, WcPt3d * bezierCurvePt,GLint ctrlNums,GLint* C,WcPt3d* ctrlPts) {
	GLint k, n = ctrlNums - 1;
	GLfloat bezierCurveFcn;

	bezierCurvePt->x = bezierCurvePt->y = bezierCurvePt->z = 0;
	for ( k = 0; k < ctrlNums; k++)
	{
		bezierCurveFcn = C[k] * pow(t, k) * pow(1 - t, n - k);
		bezierCurvePt->x += ctrlPts[k].x * bezierCurveFcn;
		bezierCurvePt->y += ctrlPts[k].y * bezierCurveFcn;
		bezierCurvePt->z += ctrlPts[k].z * bezierCurveFcn;
	}
}
// 绘制连接线段
void plotLines(WcPt3d* points, GLint Ptnums) {
	glBegin(GL_LINE_STRIP);
	for (GLint i = 0; i < Ptnums; i++)
	{
		glVertex3f(points[i].x, points[i].y, points[i].z);
	}
	glEnd();
}
// Bezier曲线的算法
void Bezier(WcPt3d *ctrlPts, GLint bezierCurveNums, GLint ctrlNums){
	WcPt3d bezierCurvePt;
	GLint k, *C; // 获取控制点的数目 
	GLfloat t;
	C = new GLint[ctrlNums]; // 获取多项式的各项系数
	
	BinomialCoeffi(C, ctrlNums - 1); // 计算各项的系数,并存储

	for ( k = 0; k <= bezierCurveNums; k++)
	{
		t = (GLfloat)k / (GLfloat)bezierCurveNums;
		computeBezierPts(t, &bezierCurvePt, ctrlNums,C,ctrlPts);
		plotPt(bezierCurvePt);
	}
	// 绘制线段
	plotLines(ctrlPts, ctrlNums);

	delete[] C;
}
// 贝塞尔曲线的升阶
void ascendingBezier(WcPt3d* oldPts,GLint oldPtnums, GLint bezierCurveNums) {
	GLint newPtNums = oldPtnums + 1;
	WcPt3d* newPts = new WcPt3d[newPtNums];
	GLint i;

	// P[i]' = i/(n+1) P[i-1] + (n+1-i)/(n+1) Pi;
	for (i = 0; i < newPtNums; i++)
	{
		GLfloat x = 0, y = 0, z= 0;
		if (i==0)
		{
			x = oldPts[i].x;
			y = oldPts[i].y;
			z = oldPts[i].z;
		}
		else if(i==newPtNums - 1){
			x = oldPts[i - 1].x;
			y = oldPts[i - 1].y;
			z = oldPts[i - 1].z;
		}
		else {
			x = (GLfloat) oldPts[i - 1].x * i / oldPtnums + oldPts[i].x * (oldPtnums - i) / oldPtnums;
			y = (GLfloat)oldPts[i - 1].y * i / oldPtnums + oldPts[i].y * (oldPtnums - i) / oldPtnums;
			z = (GLfloat)oldPts[i - 1].z * i / oldPtnums + oldPts[i].z * (oldPtnums - i) / oldPtnums;
		}
		newPts[i] = { x,y,z };
	}
	Bezier(newPts, bezierCurveNums, newPtNums);
}


// 初始化数据并进行绘制
void displayFcn1(void) {
	WcPt3d ctrlPts[7] = { {-140,-40,0}, {-60,-80,0},{90,100,0},{120,200,0},{180,160,0},{200,130,0},{230,10,0}};
	
	GLint bezierCurveNums = 2000 , ctrlNums = 7;
	glClear(GL_COLOR_BUFFER_BIT);
	glPointSize(2);
	glColor3f(0, 0, 0);
	// 将控制点,绘制精度代入
	Bezier(ctrlPts,bezierCurveNums,ctrlNums);
	
	glColor3f(1, 1, 0);
	// 升阶
	ascendingBezier(ctrlPts, ctrlNums,bezierCurveNums);

	glFlush();

}

// 对绘图窗口进行缩放
void winReshapeFcn(GLint newWidth, GLint newHeight) {
	glViewport(0, 0, newHeight, newHeight);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluOrtho2D(xwcMin, xwcMax, ywcMin, ywcMax);
	glClear(GL_COLOR_BUFFER_BIT);

}

// 主函数
int main(int argc, char **argv){
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowPosition(50, 50);
	glutInitWindowSize(600, 600);
	glutCreateWindow("贝塞尔曲线");

	init();

	glutDisplayFunc(displayFcn1);
	glutReshapeFunc(winReshapeFcn);
	glutMainLoop();
}