#include <GL/glut.h>
#include <stdlib.h>
#include <math.h>

GLfloat xwcMin = -250.0, xwcMax = 250.0;
GLfloat ywcMin = -250.0, ywcMax = 250.0;

class wcPt3D
{
public:
	GLfloat x, y, z;
};

void init(void) {
	glClearColor(1.0, 1.0, 1.0,0.0);
}

void plotPoint(wcPt3D bezCurvePt) {
	glBegin(GL_POINTS);
	glVertex2f(bezCurvePt.x, bezCurvePt.y);
	glEnd();
}

void binomialCoeffs(GLint n , GLint * C) {
	GLint k, j;
	for (k = 0; k <=n; k++)
	{
		C[k] = 1;
		for  (j = n; j >=k+1; j--)
		{
			C[k] *= j;
		}
		for (j = n-k; j >= 2; j--)
		{
			C[k] /= j;
		}
	}
}

void computeBezPt(GLfloat u, wcPt3D* bezPt, GLint nCtrlPts, wcPt3D* ctrlPts, GLint * C) {
	GLint k, n = nCtrlPts - 1;
	GLfloat bezBlenFcn;

	bezPt->x = bezPt->y = bezPt->z = 0.0;
	for (k = 0; k < nCtrlPts; k++)
	{
		bezBlenFcn = C[k] * pow(u, k) * pow(1 - u, n - k);
		bezPt->x += ctrlPts[k].x * bezBlenFcn;
		bezPt->y += ctrlPts[k].y * bezBlenFcn;
		bezPt->z += ctrlPts[k].z * bezBlenFcn;
	}
}

void bezier(wcPt3D* ctrlPts, GLint nCtrlPts, GLint nBezCurvePts) {
	wcPt3D bezCurvePt;
	GLfloat u;
	GLint *C, k;
	C = new GLint[nCtrlPts];

	binomialCoeffs(nCtrlPts - 1, C);

	for ( k = 0; k <= nBezCurvePts; k++)
	{
		u = GLfloat(k) / GLfloat(nBezCurvePts);
		computeBezPt(u, &bezCurvePt, nCtrlPts, ctrlPts, C);
		plotPoint(bezCurvePt);
	}
	delete[] C;
}

void displayFcn(void) {
	GLint nCtrlPts = 7, nBezCurvePts = 2500;
	wcPt3D ctrlPts[7] = { {-40,-40,0}, {-10,20,0},{30,-100,0},{40,40,0},{0,0,0},{50,70,0},{80,-210,0}};
	glClear(GL_COLOR_BUFFER_BIT);

	glPointSize(2);
	glColor3f(1, 0, 0);

	bezier(ctrlPts, nCtrlPts, nBezCurvePts);
	glFlush();
}
void winReshapeFcn(GLint newWidth, GLint newHeight) {
	glViewport(0, 0, newHeight, newHeight);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluOrtho2D(xwcMin, xwcMax, ywcMin, ywcMax);
	glClear(GL_COLOR_BUFFER_BIT);

}

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowPosition(50, 50);
	glutInitWindowSize(600, 600);
	glutCreateWindow("����������");

	init();
	glutDisplayFunc(displayFcn);
	glutReshapeFunc(winReshapeFcn);
	glutMainLoop();
}