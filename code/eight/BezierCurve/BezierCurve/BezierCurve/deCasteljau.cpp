#include <GL/glut.h>
#include <math.h>
#include <stdlib.h>

class WcPt3D
{
public:
	GLfloat x, y, z;
};

GLfloat xwcMin = -250.0, xwcMax = 250.0;
GLfloat ywcMin = -250.0, ywcMax = 250.0;

void plotPt(WcPt3D pt) {
	glBegin(GL_POINTS);
	glVertex3f(pt.x, pt.y, pt.z);
	glEnd();
}
// 当为1阶时
void oneCasteljau(WcPt3D* points, GLfloat t) {
	WcPt3D point;
	point.x = points[0].x * (1 - t) + points[1].x * t;
	point.y = points[0].y * (1 - t) + points[1].y * t;
	point.z = points[0].z * (1 - t) + points[1].z * t;
	plotPt(point);
}
// 当为n阶时
void nCasteljau(WcPt3D* Points, GLint n, GLfloat t) { // 绘制的点 , 阶数 = 点数 -1, t 目前的比值

	if (n > 1) 
	{
		WcPt3D* newCtrls = new WcPt3D[n]; // 获取n-1阶的点 
		GLint k;
		for ( k = 0; k < n; k++)
		{
			newCtrls[k].x = Points[k].x * (1 - t) + Points[k + 1].x * t;
			newCtrls[k].y = Points[k].y * (1 - t) + Points[k + 1].y * t;
			newCtrls[k].z = Points[k].z * (1 - t) + Points[k + 1].z * t;
		}
		nCasteljau(newCtrls, n - 1, t);
	}
	else {
		oneCasteljau(Points,t);
	}
}

// 绘制连接线段
void plotLines(WcPt3D* points, GLint Ptnums) {
	glBegin(GL_LINE_STRIP);
	for (GLint i = 0; i < Ptnums; i++)
	{
		glVertex3f(points[i].x, points[i].y, points[i].z);
	}
	glEnd();
}
// 使用算法绘制曲线
void deCasteljau(WcPt3D* ctrlPts, GLint ctrlNums, GLint bezierCurveNums) {

	GLint k;

	for ( k = 0; k <bezierCurveNums; k++)
	{
		GLfloat t = (GLfloat)k / (GLfloat)bezierCurveNums;
		nCasteljau(ctrlPts, ctrlNums - 1, t);
	}

	// 绘制线段
	plotLines(ctrlPts, ctrlNums);
}

void displayFcn() {

	WcPt3D ctrlPts[7] = { {-140,-40,0}, {-60,-80,0},{90,100,0},{120,200,0},{180,160,0},{200,130,0},{230,10,0} };

	GLint bezierCurveNums = 2000, ctrlNums = 7;

	glClear(GL_COLOR_BUFFER_BIT);
	glPointSize(2);
	glColor3f(0, 0, 0);
	
	deCasteljau(ctrlPts, ctrlNums, bezierCurveNums);
	glFlush();

}

// 对绘图窗口进行缩放
void winReshapeFcn(GLint newWidth, GLint newHeight) {
	glViewport(0, 0, newHeight, newHeight);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluOrtho2D(xwcMin, xwcMax, ywcMin, ywcMax);
	glClear(GL_COLOR_BUFFER_BIT);

}

void init() {
	glClearColor(1.0, 1.0, 1.0, 0.0);
}
int main( int argc,  char **argv) {
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowPosition(50, 50);
	glutInitWindowSize(600, 600);
	glutCreateWindow("deCasteljau使用");
	init();
	glutDisplayFunc(displayFcn);
	glutReshapeFunc(winReshapeFcn);
	glutMainLoop();
}